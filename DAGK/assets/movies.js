var maxpage=10;
var homepage=1;

const key ='bb6b429de4d526f525dabb0b3a8487bd'
async function loadPage(page){
    $('#main').empty();
      const reqStr =`https://api.themoviedb.org/3/movie/now_playing?api_key=${key}&page=${page}`;
      
      loading();
      const response =await fetch(reqStr);
      const rs = await response.json();

      fillMovies(rs.results);
      $('.pagination li:not(:last-child):not(:first-child)').remove();
      if(page === maxpage & maxpage <=rs.total_pages-2){
        maxpage=maxpage+2;
        homepage=homepage+2;
      }else{
        if(page === homepage & homepage>=2){
          maxpage=maxpage-2;
          homepage=homepage-2;
        }
      }
    for (var i = homepage; i <= maxpage; i++) {
      $('.pagination li:last-child').before(`<li class="page-item ${i === page ? "active" : ""}" >
                                          <a class="page-link" href="#" onclick=loadPage(${i})>${i}</a>
                                          </li>`)
    }
}
var trang=1;
async function evtSubmit(e){
  e.preventDefault();
  const strSearch = $('#tenfilm').val();
  
  const reqStr =`https://api.themoviedb.org/3/search/movie?api_key=${key}&query=${strSearch}`;
  loading();
  const response =await fetch(reqStr);
  const rs = await response.json();
  fillMovies(rs.results);
}
async function evtSubmit2(e){
    e.preventDefault();
    const strSearch2 = $('#tenactor').val();
    
    const reqStr2 =`https://api.themoviedb.org/3/search/person?api_key=${key}&query=${strSearch2}`;
    loading();
    const response2 =await fetch(reqStr2);
    const rs2 = await response2.json();
    fillActorMovies(rs2.results);
}

function loading(){
    $('#main').empty();
    $('#main').append(`
    <div class="d-flex justify-content-center">
        <div class="spinner-border" role="status">
            <span class="sr-only">Loading...</span>
        </div>
    </div>
  `)
}
function fillMovies(ms1){
    $('#main').empty();
    for(const m of ms1){
        $('#main').append(`
    <div class="col-md-3 py-1">
        <div class="card shadow h-100" onclick="loadDetail('${m.id}')">
            <img src="https://image.tmdb.org/t/p/w500${m.poster_path}" class="card-img-top" alt="${m.title}">
            <div class="card-body">
                <h5 class="card-title">${m.title}</h5>
                <p class="card-text">Reviews: ${m.vote_average}</p>
                <p class="card-text">Vote: ${m.vote_count}</p>
                <p class="card-text">Date: ${m.release_date}</p>
                <a href="#" class="btn btn-primary" >Detail</a>
            </div>
        </div>
    </div>
        `);
    }
    $('#main').append(`
    <nav aria-label="Page navigation example">
        <ul class="pagination">
            <li class="page-item">
                <a class="page-link" href="#" aria-label="Previous">
                    <span aria-hidden="true">&laquo;</span>
                </a>
            </li>

            <li class="page-item">
                <a class="page-link" href="#" aria-label="Next">
                    <span aria-hidden="true">&raquo;</span>
                </a>
            </li>
        </ul>
    </nav>
    `);
    } 
    function fillActorMovies(ms){
      $('#main').empty();
      for(const m of ms){
        for(const m1 of m.known_for){
          $('#main').append(`
      <div class="col-md-3 py-1">
          <div class="card shadow h-100" onclick="loadDetail('${m1.id}')">
              <img src="https://image.tmdb.org/t/p/w500${m1.poster_path}" class="card-img-top" alt="${m1.title}">
              <div class="card-body">
                  <h5 class="card-title">${m1.title}</h5>
                  <p class="card-text">Rate: ${m1.vote_average}</p>
                  <p class="card-text">Vote: ${m1.vote_count}</p>
                  <p class="card-text">Date: ${m1.release_date}</p>
                  <a href="#" class="btn btn-primary" >Detail</a>
              </div>
          </div>
      </div>
      `);
      }
    }
      $('#main').append(`
      <nav aria-label="Page navigation example">
          <ul class="pagination">
              <li class="page-item">
                  <a class="page-link" href="#" aria-label="Previous">
                      <span aria-hidden="true">&laquo;</span>
                  </a>
              </li>
  
              <li class="page-item">
                  <a class="page-link" href="#" aria-label="Next">
                      <span aria-hidden="true">&raquo;</span>
                  </a>
              </li>
          </ul>
      </nav>
      `);
    } 

async function loadDetail(id){
    const reqStr =`https://api.themoviedb.org/3/movie/${id}?api_key=${key}&append_to_response=credits`;
    const reqStr2 =`https://api.themoviedb.org/3/movie/${id}/reviews?api_key=${key}`;
    loading();
    const response =await fetch(reqStr);
    const movie =await response.json();
    const response2 =await fetch(reqStr2);
    const movie2 =await response2.json();
    fillDetail(movie);
    loadDirectors(movie.credits.crew);
    loadGenres(movie.genres);
    loadStars(movie.credits.cast);
    fillreviews(movie2.results); 
}

function fillDetail(m){
    $('#main').empty();
    $('#main').append(`
    <h1 class="my-4">${m.original_title}
    <small>${m.release_date}</small>
  </h1>

  <!-- Portfolio Item Row -->
  <div class="row">

    <div class="col-md-7">
      <img class="img-fluid" src="https://image.tmdb.org/t/p/w500${m.poster_path}" alt="">
    </div>

    <div class="col-md-5">
      <h3 class="my-3">Plot</h3>
      <p>${m.overview}</p>
      <h3 class="my-3">Directors</h3>
      <div id="directors"></div> 
      <h3 class="my-3">Genres</h3>
      <div id="genres"></div>
      <h3 class="my-3">Stars</h3>
      <div id="stars"></div> 
      <h3 class="my-3">Reviews</h3>
      <div id="reviews"></div> 

    </div>
  </div>
    `);
}
function loadDirectors(gs){
  for (var i = 0; i < gs.length; i++) {
  $('#directors').append(`
    ${gs[i].job ==='Director' ? gs[i].name : ""}
    `);
  }
}
function loadGenres(gs){
  for (var i = 0; i < gs.length; i++) {
  $('#genres').append(`
    ${gs[i].name},
    `);
  }
}
function loadStars(gs){
  for (var i = 0; i < gs.length; i++) {
  $('#stars').append(`
  <a onclick="loadStarsDetail('${gs[i].id}')">${gs[i].name}</a>,
    `);
  }
}
function fillreviews(m){
  for (const s of m )
  $('#reviews').append(`
  <p>${s.author}: ${s.content}</p><br>  
  `);
}
async function loadStarsDetail(id){
  const reqStr =`https://api.themoviedb.org/3/person/${id}?api_key=${key}&append_to_response=movie_credits`;
  loading();
  const response =await fetch(reqStr);
  const movie =await response.json();
  fillStarsDetail(movie);
  loaddsFilm(movie.movie_credits.cast);
}

function fillStarsDetail(m){
  $('#main').empty();
  $('#main').append(`
  <h1 class="my-4">${m.name}
  <small>${m.birthday}</small>
</h1>

<!-- Portfolio Item Row -->
<div class="row">

  <div class="col-md-7">
    <img class="img-fluid" src="https://image.tmdb.org/t/p/w500${m.profile_path}" alt="">
  </div>

  <div class="col-md-5">
    <h3 class="my-3">Biography</h3>
    <p>${m.biography}</p>
    <h3 class="my-3">Film</h3>
    <div id="film">
    <ul class="list-group">
    </ul>
    </div> 
  </div>
</div>
  `);
}
function loaddsFilm(gs){
  for (var i = 0; i < gs.length; i++) {
  $('#film .list-group').append(`
    <li class="list-group-item">${gs[i].title}</li>
    `);
  }
}